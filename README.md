# RenduFInal_BOUYSSOUX

## I. Objectif
L'objectif de ce projet est de créer un calculateur permettant d'effectuer les quatres opérations élémentaires sous forme de classes et packages, tout en assurant des tests fonctionnels et pertinents, et également en ajoutant une intégration continue du projet.

## II. Organisation du projet
Voici l'arborescence simplifiée du projet :
    
    .
    ├── Calculator
    │   ├── __init__.py
    │   ├── Package_Calculator
    │   │   ├── Calculator.py
    │   │   ├── __init__.py
    │   │   └── __pycache__
    │   ├── __pycache__
    │   └── TestSimpleCalculatorBx.egg-info
    ├── Package_Test
    │   ├── Calculator_test.py
    │   ├── __init__.py
    │   └── __pycache__
    │
    ├── README.md
    ├── requirements.txt
    └── setup.py


Le projet est donc composé des fichiers essentiels (README.md, .git), de fichiers permettant la bonne éxécution du projet (setup.py et requirements.txt), ainsi que deux packages:  
* Le premier est le package Calculator, qui possède un deuxième packages à l'intérieur : Package_Calculator
* Le deuxième est le Package_Test.  

#### Package_Calculator
C'est dans ce package qu'est définie le script Calculator.py qui définit sous forme d'une classe SimpleCalculator des méthodes correspondant aux opérations élémentaires (fsum, substract, multiply, divide). Ce package apparaît donc comme une librairie qu'il faudra importer afin d'utiliser les méthodes pour calculer.  
Par exemple voici la méthode fsum :  

 ![fsum](images/fsum.png)  

#### Package_Test
C'est dans ce Package que sont géré tous les tests sur les méthodes du package Calculator, permettant de tester si les méthodes fonctionnent toujours bien malgré des modifications qu'on pourrait apporter au package Calculator. Les Tests sont gérés dans le script Calculator_test.py qui définit quatre classes (une par opérande), définissant chacune toutes les possibilités et les testants, afin de vérifier que les méthodes définies dans le package calculator fonctionnent dans tous les cas. Les tests sont effectué automatiquement à l'appel du script (soit appel direct soit appel par pytest), grâce au module unittest.  
Par exemple voici l'exemple de la classe testant la méthode divide:  

 ![testdivide](images/testdivide.png)

## III. Importation des packages
Avant de pouvoir utiliser les packages ensembles, il faut importer les packages et leurs contenu, pour cela il existe trois méthodes différentes :

#### 1. PYTHONPATH
La première solution, relativement contraignante mais rapide à mettre en place et de placer le PYTHONPATH à la racine du projet, pour cela il faut ouvrir un terminal et se placer à la racine du projet et entrez la commande suivante :  

    export PYTHONPATH=$PYTHONPATH:'.'

Vous pouvez ensuite appeler les scripts d'un package à l'extérieur de celui-ci (à condition de bien renseigner le chemin vers ce script).  

#### 2. SETUP.PY
La deuxième solution est de créer un script python setup.py qui va permettre d'importer un package, pour cela il faut dans ce script indiquer le chemin d'accès à ce Package. Il faudra aussi dans certains cas définir un fichier texte requirements permettant de spécifier certaines versions de modules nécessaires à la bonne réalisation des actions.

    python setup.py sdist

    pytest

si un message d'erreur s'affiche et que le test ne s'effectue pas, alors, il faut créer un environnement virtuel et installer le fichier requirements pour pouvoir éxécuter les tests:

    sudo apt-get install python3-venv

    python3 -m venv venv #création du virtual env

    source ./venv/bin/activate #activation du virtual env

    pip install -r requirements.txt

    python setup.py sdist

    pytest

Pour sortir du virtual env, il suffit de rentrer la commande : `deactivate`.  

#### 3. Test Pypi
La dernière méthode pour installer et importer un package est de passer par la plateforme TestPypi, pour cela il faut créer un compte, créér comme précédemment un fichier setup.py et importer via internet le package souhaiter. Une fois le token créé sur le site de Pypi voici les différentes commandes à entrer :

    python3 -m pip install --user --upgrade setuptools wheel
    python3 setup.py sdist bdist_wheel

    python3 -m pip install --user --upgrade twine
    python3 -m twine upload --repository testpypi dist/*
    
    sudo apt-get install python3-venv
    python3 -m venv venv #crétation d'un virtual env (venv)
    source ./venv/bin/activate
  
    pip install -i https://test.pypi.org/simple/(nom du projet porté par votre Token) #(prendre la commande donnée sur le site test.pytp)

Seront demandés un username et un mot de passe, le login est __token__et le mot de passe est donnée sur le site.  
Pour plus d'information sur cette méthode, voici mon exerice 11 traitant le sujet avec un README un peu plus détaillé : ![ici](https://gitlab.com/PaulBx/testpipy)

## IV.  Execution des Tests
Maintenant que les packages sont bien importés et installés, on peut effectuer les tests, pour cela il existe deux possiblités pour éxecutés les tests, le lancement du script Calculator_test.py et pytest.  

#### 1. Calculator_test.py
On peut appeler directement le script, il faut cependant pour cela avoir réglé le PYTHONPATH. Dans un terminal se placer à la racine du projet et entrer la commande :  

    python3 Package_Test/Calculator_test.py

On obtient alors :  

  ![scripttest](images/scripttest.png)

On peut voir que 13 tests sont effectués avec un commentaire sur chaque test effectué permettant d'avoir la liste des tests effectués, ces commentaires ont été effectué avec le module logging.

#### 2. Pytest
La seconde méthode est d'entrer la commande `pytest` à la racine du projet. Cette commande va éxecuter tous les scripts python qui possède "test" dans leur nom.

On obtient alors :  
  
  ![pytest](images/pytest.png)

On peut voir dans ce cas là seulement le nombre de tests effectués, le pourcentage de réussite et le temps que le programme a mis pour éxecuter les tests.      

Lorsqu'un test n'est pas effectué comme il faut, par exemple si le résultat attendu n'est pas celui renvoyé par la fonction (ici imaginons que 5 - 2 vaut 4, la fonction renverra 3 il y aura donc un conflit):  

  ![pytestfaute](images/pytestfaute.png)  
  
On voit bien alors le test qui a échoué, pytest nous indique ce qui ne va pas dans le test (ici évidemment 5-2 = 3 et non pas 4 comme attendu par le programme de test).  

## V. Intégration Continue
Le projet étant enrigstrer sur Gitlab, on peut utiliser toutes les options que le site propose. L'intégration continue fait partie de ces options possibles. Cela consiste à effectuer automatiquement des actions sur le projet à chaque push du projet sur Gitlab. Dans notre cas, trois étapes sont définies à chaque push du projet sur Gitlab:  
* Analyse de la syntaxe des codes.  
* Lancement de la commande Pytest.  
* Archivage disponible au téléchargement du package Calculator.  

Pour effectuer cela, il faut créer un fichier .yml qui définit les étapes à effectuer.  
Une fois un push effectué, on peut suivre dans l'onglet CI/CD / pipeline l'éxecution de l'intégration continue, et ainsi avoir l'information sur chacune des étapes si elles ont bien été effectuées sans difficultées. Dans notre cas, il y l'archivage d'une partie du projet, il est alors une fois les étapes d'intégration continue terminée possible de télécharger l'archive via Gitlab.  
Voici à quoi ressemble l'onglet Pipeline de Gitlab, représentant l'historique de l'intégration continue :  

  ![pipeline](images/pipeline.png)

## VI. Autres Dépôts du projet
Chaque aspect du projet a été traité indépendemment sous la forme de projets Gitlab indépendants. Pour plus d'information sur un aspect précis vous pouvez lire le README de l'exerice correspondant, les liens :  
- Exercice 1 (création des méthodes pour les quatre opérations) : https://gitlab.com/PaulBx/tp1_ex1  
- Exercice 2 (création d'une classe contenant ces méthodes) : https://gitlab.com/PaulBx/tp1_ex2  
- Exercice 3 (règles de codage PEP8) : https://gitlab.com/PaulBx/tp1_ex3  
- Exercice 4 (Création Packages Calculator et Test) : https://gitlab.com/PaulBx/tp1_ex4  
- Exercice 5 (Amélioration classe Test) : https://gitlab.com/PaulBx/tp1_ex5  
- Exercice 6 (Ajout des test avec unittest) : https://gitlab.com/PaulBx/tp1_ex6  
- Exercice 7 (Ajout des logs aux tests) : https://gitlab.com/PaulBx/tp1_ex7  
- Exercice 8 (Ajout intégration continue) : https://gitlab.com/PaulBx/tp1_exo_8  
- Exercice 10 (Utilisation test.Pypi) : https://gitlab.com/PaulBx/testpipy  

## VII. Ressources
énoncé TP : https://prod.e-campus.cpe.fr/mod/wiki/view.php?id=4571

PEP8 :  
https://openclassrooms.com/fr/courses/4425111-perfectionnez-vous-en-python/4464230-assimilez-les-bonnes-pratiques-de-la-pep-8  
https://python.doctor/page-pep-8-bonnes-pratiques-coder-python-apprendre  
https://python.sdv.univ-paris-diderot.fr/15_bonnes_pratiques/  
https://about.gitlab.com/handbook/business-ops/data-team/python-style-guide/  
https://blog.impulsebyingeniance.io/outils-et-bonnes-pratiques-pour-un-code-python-de-bonne-qualite/  
https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html  

Black : https://python.doctor/page-black-code-formatter  
 
Pylint : https://realpython.com/python-code-quality/  

Intégration continue : https://gitlab.com/js-ci-training/ci-hero-unitarytest-python  

TestPypi : https://packaging.python.org/tutorials/packaging-projects/#packaging-your-project  




